/*global $, d3*/

function initOccamVizGraphsBarAvg(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "Bar with Averages",
    call:        "barAvg",
    description: "A Bar Chart with averages."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.barAvg = function(options) {
    // Sanitize Data
    options = context.OccamViz.sanitizeOptions(options);
    options.height = options.width / 3 * 2;

    // Calculate average
    var avgData = [];
    if (options.data.groups.length > 0) {
      avgData = options.data.groups[0].series.map(function (e, index) {
        var acc = 0;
        options.data.groups.forEach(function (group) {
          acc += group.series[index];
        });
        return acc / options.data.groups.length;
      });
    }

    var series = options.data.groups.map(function (e) {
      var dict = {};
      dict.name = e.name;
      dict.type = 'column';
      dict.data = e.series;
      return dict;
    }).concat({
      name: 'Average',
      type: 'spline',
      data: avgData
    });

    // Set up the chart
    var chart = new Highcharts.Chart({
      chart: {
        renderTo: $(options.selector).attr("id"),
        type: 'column',
        margin: 75,
      },

      plotOptions: {
        column: {
          depth: 25
        }
      },

      colors: options.colors,
      series: series,
    });
  };
}
