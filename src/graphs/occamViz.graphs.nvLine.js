/*global $, d3, Highcharts*/

function initOccamVizGraphsnvLine(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "nv Line Chart",
    call:        "nvLine",
    description: "An nv Line chart."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.nvLine = function(options) {
    // Sanitize Data
    options = context.OccamViz.sanitizeOptions(options);
    options.height = options.width / 3 * 2;

    var chart;
    var color_index = 0;
    var xCord = 0;
    var mydata = options.data.groups.map(function (e) {
      var dict = {};
      dict.key = e.name;
      dict.values = e.series.map(function (f) {
        var d = {};
        d.x = xCord;
        xCord++;
        if (xCord > 3) {
          xCord = 0;
        }
        d.y = f;
        return d;
      });
      dict.color = options.colors[color_index];
      color_index++;
      return dict;
    });

    nv.addGraph(function() {
      chart = nv.models.lineChart()
      .options({
        margin: {left: 100, bottom: 100},
        x: function(d,i) { return i},
        showXAxis: true,
        showYAxis: true,
        transitionDuration: 250
      });

      // chart sub-models (ie. xAxis, yAxis, etc) when accessed directly, return themselves, not the parent chart, so need to chain separately
      chart.xAxis
        .axisLabel(options.labels.x)
        .tickFormat(d3.format(',.1f'));

      chart.yAxis
        .axisLabel(options.labels.y)
        .tickFormat(d3.format(',.2f'));

      d3.select(options.selector)
        .append('svg:svg')
        .attr("class", "chart")
        .attr('preserveAspectRatio', 'xMidYMid')
        .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
        .attr('width',  options.width  + 'px')
        .attr('height', options.height + 'px')
		.style({
			width: options.width + 'px',
			height: options.height + 'px'
		})
        .datum(mydata)
        .call(chart);

      return chart;
    });
  };
}
